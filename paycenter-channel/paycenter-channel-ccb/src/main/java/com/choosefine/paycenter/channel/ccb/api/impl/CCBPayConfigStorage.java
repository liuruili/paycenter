package com.choosefine.paycenter.channel.ccb.api.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.choosefine.paycenter.common.enums.PayType;
import com.choosefine.paycenter.pay.api.AbstractPayConfigStorage;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Component
public class CCBPayConfigStorage extends AbstractPayConfigStorage {
    @Autowired
    private RedisTemplate redisTemplate;

    /**建行配置*/
    private static final String KEY_CONFIG_CHANNEL_CCB = "CONFIG_CHANNEL_CCB";
    /**建行公共配置key*/
    private static final String KEY_COMMON = "COMMON";
    /**建行B2C网银支付配置key*/
    private static final String KEY_B2C_NB = "B2C_NB";
    /**建行B2C APP支付配置key*/
    private static final String KEY_B2C_APP = "B2C_APP";
    /**建行B2B网银支付配置key*/
    private static final String KEY_B2B_NB = "B2B_NB";
    /**建行外联平台配置key*/
    private static final String KEY_WLPT = "WLPT";

    /**公共配置*/
    private JSONObject common;
    /**b2c网银支付配置*/
    private JSONObject b2cNB;
    /**b2b网银支付配置*/
    private JSONObject b2bNB;
    /**b2c app支付配置*/
    private JSONObject b2cApp;
    /**外联平台配置*/
    private JSONObject wlpt;


    @PostConstruct
    public void init(){
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection redisConnection) throws DataAccessException {
                Map<byte[], byte[]> map = redisConnection.hGetAll(KEY_CONFIG_CHANNEL_CCB.getBytes());
                common = JSON.parseObject(new String(map.get(KEY_COMMON.getBytes())));
                //初始化商户id
                setMerchantId(common.getString(MERCHANT_ID));
                b2cNB = JSON.parseObject(new String(map.get(KEY_B2C_NB.getBytes())));
                b2cApp = JSON.parseObject(new String(map.get(KEY_B2C_APP.getBytes())));
                b2bNB = JSON.parseObject(new String(map.get(KEY_B2B_NB.getBytes())));
                wlpt = JSON.parseObject(new String(map.get(KEY_WLPT.getBytes())));
                redisConnection.close();
                return null;
            }
        });
        setPayType("CCB");
        setSignType("MD5");
    }

    /**
     * 如果存在不同的交易方式需要不同的公钥，那么使用此方法
     * @param payType
     * @return
     */
    public String getPublicKey(PayType payType) {
        if(CCBPayType.B2C_NB.getType().equals(payType.getType())){
            return b2cNB.getString(PUBLIC_KEY);
        }else if(CCBPayType.B2C_APP.getType().equals(payType.getType())){
            return b2cApp.getString(PUBLIC_KEY);
        }else{
            return b2bNB.getString(PUBLIC_KEY);
        }
    }

    /**
     * 此方法不支持
     * @return
     */
    public String getPublicKey() {
       throw new UnsupportedOperationException();
    }


    @Override
    public String getValue(PayType payType,String key) {
       if(CCBPayType.B2C_NB.getType().equals(payType.getType())){
            return b2cNB.getString(key);
       }else if(CCBPayType.B2C_APP.getType().equals(payType.getType())){
           return b2cApp.getString(key);
       }else{
           return b2bNB.getString(key);
       }
    }
}
