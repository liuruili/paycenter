package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W1101TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：（6W1101）账号、户名是否匹配校验交易
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W1101TxReq extends CCBTxReq {
    @XStreamAlias("TX_INFO")
    private CCB6W1101TxReqBody txBody;

    public CCB6W1101TxReq(){
        setTX_CODE("6W1101");
    }
}
