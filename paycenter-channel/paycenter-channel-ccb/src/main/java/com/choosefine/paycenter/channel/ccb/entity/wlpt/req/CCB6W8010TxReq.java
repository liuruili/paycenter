package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W8010TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.10 （6W8010）行内单笔转账
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W8010TxReq extends CCBTxReq {
    @XStreamAlias("TX_INFO")
    private CCB6W8010TxReqBody txBody;

    public CCB6W8010TxReq(){
        setTX_CODE("6W8010");
    }

    /** 18 SIGN_INFO 签名信息 varChar(254) T 　*/
    private String SIGN_INFO="";
    /** 19 SIGNCERT 签名CA信息 varChar(254) T 客户采用socket连接时，建行客户端自动添加*/
    private String SIGNCERT="";
}

