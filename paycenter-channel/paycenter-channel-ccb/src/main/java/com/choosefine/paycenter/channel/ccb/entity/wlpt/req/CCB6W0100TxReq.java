package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W0100TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Comments：NO.2 （6W0100）余额查询交易 Request
 * Author：Jay Chang
 * Create Date：2017/4/20
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class CCB6W0100TxReq extends CCBTxReq {
    @XStreamAlias("TX_INFO")
    private CCB6W0100TxReqBody txBody;

    public CCB6W0100TxReq(){
        setTX_CODE("6W0100");
    }
}
