package com.choosefine.paycenter;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.CCB6W1101TxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W1101TxReqBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.CCB6W1101TxRes;
import com.choosefine.paycenter.channel.ccb.utils.CCBWlptUtils;
import com.choosefine.paycenter.common.utils.PasswordUtils;
import com.choosefine.paycenter.common.utils.RequestUtils;
import com.choosefine.paycenter.common.utils.XmlUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
//@SpringBootTest
//@RunWith(SpringJUnit4ClassRunner.class)
public class CCBWlptUtilsTest {
    @Autowired
    private CCBWlptUtils ccbWlptUtils;

    @Test
    public void postForCCBWlpt() throws Exception {
//        CCB6W1101TxReq ccb6W1101TxReq = new CCB6W1101TxReq();
//        ccb6W1101TxReq.setCUST_ID("ZJ33000009170851001");
//        ccb6W1101TxReq.setLANGUAGE("CN");
//        ccb6W1101TxReq.setREQUEST_SN("10010034");
//        ccb6W1101TxReq.setPASSWORD("781220");
//        ccb6W1101TxReq.setTX_CODE("6W1101");
//        ccb6W1101TxReq.setUSER_ID("WLPT01");
//
//        CCB6W1101TxReqBody txBody = new CCB6W1101TxReqBody();
//        txBody.setACC_NAME("邓友谊");
//        txBody.setACC_NO("6217002920110610632");
//
//        ccb6W1101TxReq.setTxBody(txBody);
//
//        System.out.println(XmlUtils.getInstance().toXml(ccb6W1101TxReq));
//        CCB6W1101TxRes ccb6W1101TxRes = ccbWlptUtils.postForCCBWlpt(ccb6W1101TxReq, CCB6W1101TxRes.class);
//        System.out.println(XmlUtils.getInstance().toXml(ccb6W1101TxRes));

        String xml = "<?xml version=\"1.0\" encoding=\"GB2312\"?><TX><REQUEST_SN>10010035</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W1101</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><ACC_NO>6217002920110610632</ACC_NO><ACC_NAME>邓友谊</ACC_NAME></TX_INFO></TX>";
        CCB6W1101TxReq ccb6W1101TxReq1 = XmlUtils.getInstance().fromXml(xml, CCB6W1101TxReq.class);
        ccbWlptUtils = new CCBWlptUtils();
        String resXML = ccbWlptUtils.postForString("http://192.168.59.96:12345",xml);
        //CCB6W1101TxRes ccb6W1101TxRes = ccbWlptUtils.postForCCBWlpt(ccb6W1101TxReq1, CCB6W1101TxRes.class);
        System.out.println(resXML);
    }

    public void postForCCB6W8010() throws Exception {

        ccbWlptUtils = new CCBWlptUtils();

        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");

        //行内单笔转账
        //中国建设银行(麓谷支行)
        //
        String recvName = "";
        String recvAccNo = "";
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\" standalone=\"yes\" ?><TX><REQUEST_SN>" + System.currentTimeMillis() + "</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8010</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>" + recvAccNo + "</RECV_ACCNO><RECV_ACC_NAME>" + recvName + "</RECV_ACC_NAME><CHK_RECVNAME>1</CHK_RECVNAME><RECV_OPENACC_DEPT>1</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><CST_PAY_NO>10017</CST_PAY_NO><USEOF>settlement</USEOF><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String responseXml = ccbWlptUtils.postForString("http://127.0.0.1:12345", requestXml);
        System.out.println(responseXml);
    }

    @Test
    public void postForCCB6W0100() {
        ccbWlptUtils = new CCBWlptUtils();

        ccbWlptUtils.setWlptUrl("http://api-pay-test.sxiaobao.com");

        //查询余额
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\" standalone=\"yes\" ?><TX><REQUEST_SN>1020</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W0100</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><ACC_NO>33050161772700000354</ACC_NO></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForString("http://api-pay.sxiaobao.com",requestXml);
        System.out.println(responseXml);
    }

    @Test
    public void postForCCB6W8060() {
        ccbWlptUtils = new CCBWlptUtils();

        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");
        //跨行单笔转账（超级网银）
        String recvAcctNo = "";
        String recvName = "";
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\" standalone=\"yes\" ?><TX><REQUEST_SN>" + System.currentTimeMillis() + "</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8060</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><TRAN_TYPE>0</TRAN_TYPE><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>6228270340963461623</RECV_ACCNO><RECV_ACC_NAME>张洁</RECV_ACC_NAME><RECV_UBANKNO>103100000026</RECV_UBANKNO><RECV_OPENACC_DEPT>海宁长安支行</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><USEOF>settlement</USEOF><CST_PAY_NO>132232</CST_PAY_NO></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String responseXml =ccbWlptUtils.postForString("http://127.0.0.1:12345", requestXml);
        System.out.println(responseXml);
    }

    @Test
    public void postForCCB6W0600() {
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");

        //（6W0600）转账交易结果查询
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\" standalone=\"yes\" ?><TX><REQUEST_SN>"+System.currentTimeMillis()+"</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W0600</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><REQUEST_SN1>2017051300000083</REQUEST_SN1></TX_INFO></TX>";
        //String responseXml = RequestUtils.getInstance().postForString("http://127.0.0.1:12345", requestXml);
        String responseXml =ccbWlptUtils.postForString("http://jaychang.wicp.net:43308", requestXml);
        System.out.println(responseXml);
    }

    @Test
    public void postFor5W1002() {
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");

        //（5W1002）商户支付流水查询
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\"?><TX><REQUEST_SN>1000236</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>5W1002</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><START /><STARTHOUR /><STARTMIN /><END /><ENDHOUR /><ENDMIN /><KIND>1</KIND><ORDER>170421085804106700000636</ORDER><ACCOUNT /><DEXCEL>1</DEXCEL><MONEY /><NORDERBY>1</NORDERBY><PAGE>1</PAGE><POS_CODE /><STATUS>1</STATUS></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForString("http://testing.imwork.net:26917", requestXml);
        System.out.println(responseXml);
    }

    public void postFor5W1001() {
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");

        //（5W1001）B2C外联启动连接交易
        String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\" standalone=\"yes\"?><TX><REQUEST_SN>11001</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>5W1004</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><REM1></REM1><REM2></REM2></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForString("http://127.0.0.1:12345", requestXml);
        System.out.println(responseXml);
    }

    public void postForXX() {
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");
        String recvAcctNo = "";
        String recvName = "";
        //String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\" standalone=\"yes\"?><TX><REQUEST_SN>1021</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8010</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>6217001430014758355</RECV_ACCNO><RECV_ACC_NAME>张洁</RECV_ACC_NAME><CHK_RECVNAME>1</CHK_RECVNAME><RECV_OPENACC_DEPT>海宁长安支行</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><CST_PAY_NO>10017</CST_PAY_NO><USEOF>withdraw</USEOF><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        //String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\"?><TX><REQUEST_SN>1705061319073076</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8010</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>6217002920110610632</RECV_ACCNO><RECV_ACC_NAME>邓友谊</RECV_ACC_NAME><CHK_RECVNAME>1</CHK_RECVNAME><RECV_OPENACC_DEPT>CCB</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><USEOF>提现</USEOF><CST_PAY_NO>170506131907307600000036</CST_PAY_NO><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String requestXml =  "<?xml version=\"1.0\" encoding=\"GB2312\"?><TX><REQUEST_SN>2017051000000020</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8010</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>6217001430014758355</RECV_ACCNO><RECV_ACC_NAME>yhh</RECV_ACC_NAME><CHK_RECVNAME>1</CHK_RECVNAME><RECV_OPENACC_DEPT>CCB</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><USEOF>提现</USEOF><CST_PAY_NO>20170510101549306800000056</CST_PAY_NO><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String responseXml = ccbWlptUtils.postForString("http://192.168.59.96:12345", requestXml);
        System.out.println(responseXml);
    }


    public void testQY6W8010() {
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");
        //行内单笔转账(企业小额)
        //String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\"?><TX><REQUEST_SN>"+System.currentTimeMillis()+"</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8010</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>33050161772700000616</RECV_ACCNO><RECV_ACC_NAME>杭州和村娱乐管理有限公司</RECV_ACC_NAME><CHK_RECVNAME>1</CHK_RECVNAME><RECV_OPENACC_DEPT>中国建设银行股份有限公司杭州经济技术开发区支行XXXX</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><CST_PAY_NO></CST_PAY_NO><USEOF>提现</USEOF><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\"?><TX><REQUEST_SN>2017051500000120</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8010</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>6217002920110610632</RECV_ACCNO><RECV_ACC_NAME>邓友谊</RECV_ACC_NAME><CHK_RECVNAME>1</CHK_RECVNAME><RECV_OPENACC_DEPT>中国建设银行</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><USEOF>提现</USEOF><CST_PAY_NO>20170515220020347600000135</CST_PAY_NO><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String responseXml = ccbWlptUtils.postForString("http://192.168.59.96:12345", requestXml);
        System.out.println(responseXml);
    }

    public void testQY6W8020(){
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");
        //跨行单笔转账大小额（企业小额）
        String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\"?><TX><REQUEST_SN>"+System.currentTimeMillis()+"</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8020</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>33050161772700000616</RECV_ACCNO><RECV_ACC_NAME>杭州和村娱乐管理有限公司</RECV_ACC_NAME><RECV_OPENACC_DEPT>中国建设银行股份有限公司杭州经济技术开发区支行</RECV_OPENACC_DEPT><RECV_UBANKNO>2223333</RECV_UBANKNO><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><USEOF>验证</USEOF><CST_PAY_NO>10170</CST_PAY_NO><REM1></REM1><REM2></REM2></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";

        String responseXml = ccbWlptUtils.postForString("http://192.168.59.96:12345", requestXml);

        System.out.println(responseXml);
    }


    public void testQY6W8060(){
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");
        //跨行单笔转账超级网银（企业小额）
        String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\"?><TX><REQUEST_SN>"+System.currentTimeMillis()+"</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W8060</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><TRAN_TYPE>0</TRAN_TYPE><PAY_ACCNO>33050161772700000354</PAY_ACCNO><RECV_ACCNO>33050161772700000616</RECV_ACCNO><RECV_ACC_NAME>杭州和村娱乐管理有限公司</RECV_ACC_NAME><RECV_UBANKNO>105331009007</RECV_UBANKNO><RECV_OPENACC_DEPT>中国建设银行股份有限公司杭州经济技术开发区支行</RECV_OPENACC_DEPT><AMOUNT>0.01</AMOUNT><CUR_TYPE>01</CUR_TYPE><USEOF>验证</USEOF><CST_PAY_NO>10170</CST_PAY_NO></TX_INFO><SIGN_INFO></SIGN_INFO><SIGNCERT></SIGNCERT></TX>";
        String responseXml = ccbWlptUtils.postForString("http://192.168.59.96:12345",requestXml);
        System.out.println(responseXml);
    }


    public void test6WY101(){
        ccbWlptUtils = new CCBWlptUtils();
        ccbWlptUtils.setWlptUrl("http://127.0.0.1:12345");
        //NO.1 （6WY101）一点接入活期账户明细查询
        String requestXml = "<?xml version=\"1.0\" encoding=\"gb2312\" standalone=\"yes\"?><TX><REQUEST_SN>"+System.currentTimeMillis()+"</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6WY101</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><ACCNO1>2017051000000034</ACCNO1><STARTDATE>201705130037</STARTDATE><ENDDATE>201705130057</ENDDATE><BARGAIN_FLAG></BARGAIN_FLAG><CHECK_ACC_NO></CHECK_ACC_NO><CHECK_ACC_NAME></CHECK_ACC_NAME><REMARK></REMARK><LOW_AMT></LOW_AMT><HIGH_AMT></HIGH_AMT><PAGE></PAGE><POSTSTR></POSTSTR><TOTAL_RECORD></TOTAL_RECORD><DET_NO></DET_NO></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForString("http://192.168.59.96:12345",requestXml);
        System.out.println(responseXml);
    }



}