package com.choosefine.paycenter.channel.abc.entity;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-04-27 16:56
 **/
public class ABCCommodityType {
    public static final String ABC_COMMODITY_RECHARGE_ACCOUNT_RECHARGE = "0101";//充值类：支付账户充值
    public static final String ABC_COMMODITY_CONSUME_VIRTUAL = "0201";//消费类：虚拟类
    public static final String ABC_COMMODITY_CONSUME_TRADITION = "0202";//消费类：传统类
    public static final String ABC_COMMODITY_CONSUME_REALNAME = "0203";//消费类：实名类
    public static final String ABC_COMMODITY_TRANSFER_ABC = "0301";//转账类：本行转账
    public static final String ABC_COMMODITY_TRANSFER_OTHER = "0302";//转账类：他行转账
}
