package com.choosefine.paycenter.channel.abc.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦 2017下午3:22:57
 */
@Getter
@Setter
@ToString
public class ABCOrderitems {
	private String SubMerName=""; // 二级商户名称 非必须
	private String SubMerId=""; // 二级商户代码 非必须
	private String SubMerMCC=""; // 二 级 商 户MCC 码非必须
	private String SubMerchantRemarks="";// 二级商户备注 非必须
	private String ProductID=""; // 商品代码 非必须
	private String ProductName=""; // 商品名称必须设定（如果有orderitems，则该项必须设定）
	private String UnitPrice=""; // 商品总价 非必须
	private String Qty=""; // 商品数量 非必须
	private String ProductRemarks=""; // 商品备注 非必须
	private String ProductType=""; // 商品类型 非必须
	private String ProductDiscount=""; // 商户折扣 非必须
	private String ProductExpiredDate=""; // 商品有效期 非必须
}
