package com.choosefine.paycenter.common.exception;

/**
 * Created by Jay Chang on 2017/3/6.
 */
public class BusinessException extends RuntimeException{
    private int code;
    public BusinessException(){super();}

    public BusinessException(int code,String message ,Throwable cause){
        super(message,cause);
        this.code = code;
    }

    public BusinessException(int code,String message){
        super(message);
        this.code = code;
    }

    public BusinessException(int code, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
