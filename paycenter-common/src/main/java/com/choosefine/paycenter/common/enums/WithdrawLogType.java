package com.choosefine.paycenter.common.enums;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-10 11:32
 **/
public enum WithdrawLogType implements BaseEnum {
    WAITTING_BANK_DEAL("等待银行处理"),BANK_ERROR("银行返回的错误"), SUCCESS("成功"), BUSINESS_ERROR("银行成功，业务失败");
    private String name;

    WithdrawLogType(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    }
