package com.choosefine.paycenter.settlement.model;

import com.choosefine.paycenter.common.enums.WithdrawLogType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-10 11:29
 **/
@Getter
@Setter
@ToString
@Table(name = "paycenter_withdraw_log")
public class WithdrawLog {
    private String wSn;
    private String requestSn;
    private String accountName;
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private WithdrawLogType logType;
    private String message;
    /**创建时间*/
    private Timestamp createdTime;
    /**最后修改时间*/
    private Timestamp updatedTime;
}
