package com.choosefine.paycenter.settlement.model;

import com.choosefine.paycenter.common.enums.WithdrawStatus;
import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 充值提现
 * Created by Jay Chang on 2017/3/4.
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_withdraw")
public class Withdraw extends BaseModel implements Serializable {
    /**提现流水号*/
    private String wSn;
    /**资金账号id*/
    private Long accountId;
    /**资金账户真实姓名或公司名称*/
    private String accountRealName;
    /**充值或提现金额（单位：分）*/
    private BigDecimal amount;
    /**资金账户银行卡关联关系表id*/
    private Long accountBankcardId;
    /**持卡人真实姓名*/
    private String bankcardName;
    /**银行卡卡号*/
    private String bankcardNo;
    /**提现操作人*/
    private String operName;
    /**提现状态*/
    @Enumerated(EnumType.STRING)
    private WithdrawStatus status;
    /**创建时间*/
    private Timestamp createdTime;
    /**最后修改时间*/
    private Timestamp updatedTime;
    /**请求银行的request_sn流水号,可用此流水号去查询提现单的状态(即转账结果查询时使用)*/
    private String bankRequestSn;

    private String wPayBankCode;

}
