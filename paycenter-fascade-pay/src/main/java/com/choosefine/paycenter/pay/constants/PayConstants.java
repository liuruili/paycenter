package com.choosefine.paycenter.pay.constants;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayConstants {
    /**支付Topic tag PAID*/
    public final static String PAY_TAG_PAY_SUCCESS = "PAY_SUCCESS";
    /**支付失败Topic tag PAID_FAIL*/
    public final static String PAY_TAG_PAY_FAILURE = "PAY_FAILURE";
}
