package com.choosefine.paycenter.pay.vo;

import com.choosefine.paycenter.common.enums.BizzSys;
import lombok.*;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/6/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PayFromAccountBillResultVo {
    /**业务系统标识*/
    private BizzSys bizzSys;
    /**业务系统流水号*/
    private String bizzSn;
}
