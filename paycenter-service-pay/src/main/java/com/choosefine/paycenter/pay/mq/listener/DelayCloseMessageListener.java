package com.choosefine.paycenter.pay.mq.listener;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.choosefine.paycenter.account.model.Recharge;
import com.choosefine.paycenter.account.service.AccountBillService;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.RechargeStatus;
import com.choosefine.paycenter.common.utils.ByteArrayStringUtils;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.api.RechargeService;
import com.choosefine.paycenter.pay.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component("delayCloseMessageListener")
public class DelayCloseMessageListener implements MessageListener {
    @Autowired
    private SerialNumberUtils serialNumberUtils;

    @Autowired
    private AccountBillService accountBillService;

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        final byte[] msgBody = message.getBody();
        final String tmpStr = ByteArrayStringUtils.getInstance().byteArray2String(msgBody,"UTF-8");
        final String[] tmpStrArr = tmpStr.split("_");
        BizzSys bizzSys = BizzSys.valueOf(tmpStrArr[0]);
        String orderSn = tmpStrArr[1];
        //关闭账单
        accountBillService.closeAccountBill(bizzSys,orderSn);
        return Action.CommitMessage;
    }
}
