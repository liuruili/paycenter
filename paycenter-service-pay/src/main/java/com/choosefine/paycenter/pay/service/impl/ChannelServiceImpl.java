package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.pay.dao.ChannelMapper;
import com.choosefine.paycenter.pay.model.Channel;
import com.choosefine.paycenter.pay.service.ChannelService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Administrator on 2017/4/18.
 */
@Slf4j
@Service
public class ChannelServiceImpl implements ChannelService {
    @Autowired
    private ChannelMapper channelMapper;

    private List<Channel> channelList;

    @PostConstruct
    public void init(){
        channelList = findChannelListDistinct();
    }

    @Override
    public String findLogoByChannelCode(String channelCode){
        Assert.notNull(channelCode,"param channelCode is required");
        if(CollectionUtils.isNotEmpty(channelList)){
            for(Channel channel : channelList){
                if(channelCode.equalsIgnoreCase(channel.getChannelCode())){
                    return channel.getLogo();
                }
            }
        }
        return null;
    }

    @Override
    public String findLogoByBankCode(String bankCode){
        Assert.notNull(bankCode,"param bankCode is required");
        if(CollectionUtils.isNotEmpty(channelList)){
            for(Channel channel : channelList){
                if(bankCode.equalsIgnoreCase(channel.getBankCode())){
                    return channel.getLogo();
                }
            }
        }
        return null;
    }

    public List<Channel> findChannelList(String payType){
        return channelMapper.selectChannelList(payType);
    }

    @Override
    public List<Channel> findAllChannelList() {
        return channelMapper.selectAllChannelList();
    }

    public String findChannelNameByCode(String channelCode) {
        return channelMapper.selectChannelNameByCode(channelCode);
    }

    @Override
    public String findBankCodeById(Long id) {
        return channelMapper.selectBankCodeById(id);
    }

    @Override
    public String findChannelListDistinct(String bankCode) {
        return channelMapper.selectChannelListDis(bankCode);
    }

    @Override
    public String findChannelCodeByBankCode(String bankCode) {
        return channelMapper.selectChannelCodeByBankCode(bankCode);
    }

    @Override
    public Channel findChannelByChannelCode(String channelCode) {
        Assert.notNull(channelCode,"param channelCode is required");
        if(CollectionUtils.isNotEmpty(channelList)){
            for(Channel channel : channelList){
                if(channelCode.equalsIgnoreCase(channel.getChannelCode())){
                    return channel;
                }
            }
        }
        throw new RuntimeException("can not find the channelCode["+channelCode+"] 's data,please insert one row data to database");
    }

    @Override
    public Channel findChannelByBankCode(String bankCode) {
        Assert.notNull(bankCode,"param bankCode is required");
        if(CollectionUtils.isNotEmpty(channelList)){
            for(Channel channel : channelList){
                if(bankCode.equalsIgnoreCase(channel.getBankCode())){
                    return channel;
                }
            }
        }
        throw new RuntimeException("can not find the bankCode["+bankCode+"] 's data,please insert one row data to database");
    }

    @Override
    public List<Channel> findChannelListDistinct() {
        return channelMapper.selectAllDistinctChannelList();
    }
}
