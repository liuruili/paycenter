package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.dao.PaymentToTradeOrderMapper;
import com.choosefine.paycenter.pay.model.PaymentToTradeOrder;
import com.choosefine.paycenter.pay.service.PaymentToTradeOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 20:42
 **/
@Slf4j
@Service
public class PaymentToTradeOrderServiceImpl implements PaymentToTradeOrderService {
    @Autowired
    PaymentToTradeOrderMapper paymentToTradeOrderMapper;

    @Autowired
    private SerialNumberUtils serialNumberUtils;

    @Override
    public int recordPaymentToTradeOrder(PaymentDto paymentDto, String paySn) {
        PaymentToTradeOrder paymentToTradeOrder=null;
        int count=0;
        for (int i=0;i<paymentDto.getOrders().size();i++){
            paymentToTradeOrder = new PaymentToTradeOrder();
            OrderDto orderDto = paymentDto.getOrders().get(i);
            paymentToTradeOrder.setBizzSys(paymentDto.getBizzSys());
            //转账的时候，业务流水号与业务订单号是相同的，故有以下处理方案
            if(serialNumberUtils.isTransferBizzSn(paymentDto.getBizzSn())){
                paymentToTradeOrder.setOrderSn(paymentDto.getBizzSn());
            }else{
                paymentToTradeOrder.setOrderSn(orderDto.getOrderSn());
            }
            paymentToTradeOrder.setPaySn(paySn);
            int j = paymentToTradeOrderMapper.insertSelective(paymentToTradeOrder);
            count+=j;
        }
        return count;
    }

    @Override
    public int savePaymentToTradeOrder(PaymentToTradeOrder paymentToTradeOrder) {
        return paymentToTradeOrderMapper.insertSelective(paymentToTradeOrder);
    }

    @Override
    public List<String> selectOrderSnByPaySn(String paySn) {
        return paymentToTradeOrderMapper.selectOrderSnByPaySn(paySn);
    }

    @Override
    public String selectPaySnByBizzSysAndOrderSn(String bizzSys, String orderSn) {
        return paymentToTradeOrderMapper.selectPaySnByBizzSysAndOrderSn(bizzSys,orderSn);
    }

    @Override
    public PaymentToTradeOrder findByPaySn(String paySn) {
        return paymentToTradeOrderMapper.selectByPaySn(paySn);
    }
}
