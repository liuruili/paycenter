package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.BankUnion;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 14:37
 **/
public interface BankUnionMapper extends BaseMapper<BankUnion> {
    @Select("SELECT * FROM paycenter_bank_union WHERE bank_code=#{bankCode} and area_code=#{cityCode}")
    public List<BankUnion> selectBankUnionByCityCodeAndBankCode(@Param("bankCode") String bankCode, @Param("cityCode") String cityCode);
}
