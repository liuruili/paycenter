package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.model.ShiXiaoBaoTrx;
import com.choosefine.paycenter.common.dao.BaseMapper;

/**
 * Created by Jay Chang on 2017/3/7.
 */
public interface ShiXiaoBaoTrxMapper extends BaseMapper<ShiXiaoBaoTrx> {
}
