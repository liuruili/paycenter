package com.choosefine.paycenter.account.enums;

import com.choosefine.paycenter.common.enums.BaseEnum;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum AccountStatus implements BaseEnum {
    /**正常*/
    NORMAL("正常使用中"),
    /**冻结*/
    BLOCKED("已冻结");

    private String name;

    AccountStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
