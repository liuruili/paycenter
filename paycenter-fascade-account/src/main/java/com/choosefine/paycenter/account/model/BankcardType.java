package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/28
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class BankcardType extends BaseModel implements Serializable{
    private String bankcardTypeCode;
    private String bankcardTypeName;
    private String bankName;
    private String bankLogo;
}
