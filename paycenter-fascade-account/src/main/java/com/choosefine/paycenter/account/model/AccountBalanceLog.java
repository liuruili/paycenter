package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 账户余额变动日志
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_account_balance_log")
public class AccountBalanceLog extends BaseModel implements Serializable {
    /**账户id*/
    private Long accountId;
    /**账户真实名*/
    private String accountRealName;
    /**租户编码*/
    private String accountUserCode;
    /**旧的冻结余额*/
    private BigDecimal oldFreezeBalance;
    /**旧的可用余额*/
    private BigDecimal oldAvailableBalance;
    /**旧的总余额*/
    private BigDecimal oldBalance;
    /**新的冻结余额*/
    private BigDecimal newFreezeBalance;
    /**新的可用余额*/
    private BigDecimal newAvailableBalance;
    /**新的总余额*/
    private BigDecimal newBalance;
    /**变更说明*/
    private String changeDesc;
}
