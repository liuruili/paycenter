package com.choosefine.paycenter.account.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@ApiModel("充值Dto")
@Getter
@Setter
@ToString
public class RechargeDto {
    /**租户编码*/
    @ApiModelProperty("租户编码")
    private String userCode;
    /**操作员*/
    @ApiModelProperty("操作员")
    private String operName;
    /**充值完成后，显示的页面url*/
    @URL
    private String returnUrl;
    @ApiModelProperty("充值金额")
    @NotNull
    @NumberFormat
    private BigDecimal amount;
    /**充值流水*/
    private String rSn;
    /**支付通道*/
    @ApiModelProperty("充值使用的支付通道")
    @NotNull
    private String channel;
    /**支付类型*/
    @ApiModelProperty("支付类型")
    @NotNull
    private String payType;
}
