package com.choosefine.paycenter.account.service;

/**
 * Comments：(锁定或解锁)账户服务
 * Author：Jay Chang
 * Create Date：2017/3/29
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface AccountLockService {


    public static final String ACCOUNT_LOCK_PREFIX = "ACCT_LOCK_";

    public static final String ACCOUNT_KEY_PREFIX = "ACCT_PAY_PASS_INPUT_ERR_";


    /**
     * 校验账户是否被锁定
     * @param id
     * @return
     */
    public boolean isLockAccount(Long id);

    /**
     * 锁定账户10分钟
     * @param id
     * @return
     */
    public void lockAccount(Long id);

    /**
     * 解除锁定账户
     * @param id
     * @return
     */
    public void unlockAccount(Long id);

}
